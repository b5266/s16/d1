// Repetition Control Structures

// While Loop
/*Syntax
	While (condition){
		statement/s;
	}
*/
/*
let count = 5;

// count = 4
while(count !==0){
	console.log("While: " + count);
	count--;
}

console.log("Displays number 1-10");
count = 1;

while(count <= 10){
	console.log("While: " + count);
	count++;
}
*/
// Do while Loop
/*Syntax
	do{
		statement;
	}while (condition);
*/


let number = Number(prompt("Give me a number"));

do{
	console.log("Do While: " + number);
	number += 1;
}while (number < 10);

// 

let even = 2

do{
	console.log("Do While: " + even);
	even += 2;
}while (even <= 10);








console.log("For Loop");

for(let count = 0; count <= 20; count ++){
	console.log(count);
}

console.log("Even For loop");
let evenNum = 2;
for(let counter = 1; counter <= 5; counter ++){
	console.log("Even: " + evenNum);
	evenNum += 2
}

/*
	counter = 1, 2, 3, 4, 5, 6
	even = 2, 4, 6, 8, 10, 12
*/
/*Expected output:
	Even: 2
	Even: 4
	Even: 6
	Even: 8
	Even: 10
*/

console.log("Other loop examples:");
let myString = 'alex';
// .length property is used to count the characters in a string
console.log(myString.length);

console.log(myString[0]);
console.log(myString[3]);

console.log("for loop example:")

for(let x = 0; x < myString.length; x++){
	console.log(myString[x])
}

/*Expected Output
	a
	l
	e
	x
*/
/*For Reverse example
	for(let x = 3; x >= 0; x--){
		console.log(myString[x]);
	}
  
*/

//
let myName = "AlEx";
for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		){
			console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}

// Continue and Break Statements

for(let count = 0; count <= 20; count++){
	// if the remainder is equal to zero, tells the code to continue to iterate
	if(count % 2 === 0){
			continue;
	}
	console.log("Continue and Break: " + count);

	//
	if(count > 10){
		break;
	}
}

let name = "Alexandro"
/*
	name = 9
	i = 0, 1, 2, 3, 4
*/

for(let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
	continue;
	}
	if(name[i] === "d"){
		break;
	}
}